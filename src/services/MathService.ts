import { makeAutoObservable } from "mobx";

class MathService {
  result = 0;
  temp = "";
  operation = "";
  first = 0;
  flag = false;
  second = 0;
  pile = "";
  counts = [];
  tempCount = "";
  signs = [];

  constructor() {
    makeAutoObservable(this);
  }

  reset() {
    this.result = 0;
    this.temp = "";
    this.flag = false;
    this.pile = "";
  }
  setCount(a: string) {
    if (this.pile.slice(-1) === "=") {
      this.pile = this.temp;
    }
    this.pile = this.pile.concat(a.toString());
    if (a === "+" || a === "-" || a === "/" || a === "x") {
      this.operation = a;
      if (!this.flag) {
        this.first = Number(this.temp);
        this.flag = true;
        this.temp = "";
      } else {
        this.second = Number(this.temp);
      }
    } else {
      if (!this.flag) {
        this.temp = this.temp.concat(a.toString());
        this.result = Number(this.temp);
      } else {
        this.temp = this.temp.concat(a.toString());
        this.result = Number(this.temp);
      }
    }
  }
  calculate() {
    this.second = Number(this.temp);
    this.pile = this.pile.concat("=");
    switch (this.operation) {
      case "+":
        this.result = this.first + this.second;
        break;
      case "-":
        this.result = this.first - this.second;
        break;
      case "/":
        this.result = this.first / this.second;
        break;
      case "x":
        this.result = this.first * this.second;
        break;
    }
    this.flag = false;
    this.temp = this.result.toString();
  }
}

export const mathService = new MathService();

export default MathService;
