import React, { VFC } from "react";
import styled from "styled-components";

interface ButtonProps {
  onClick: () => void;
  children: string;
  span: number;
  colour: string;
  fontColour: string;
}

const Button: VFC<ButtonProps> = ({ onClick, span, children, colour, fontColour }) => {
  return (
    <StyledButton fontColour={fontColour} span={span} onClick={onClick} colour={colour}>
      {children}
    </StyledButton>
  );
};

interface StyledButtonProps {
  span: number;
  colour: string;
  fontColour: string;
}

const StyledButton = styled.button<StyledButtonProps>`
  height: 35px;
  color: ${StyledButtonPros => StyledButtonPros.fontColour};
  font-size: 20px;
  background: ${StyledButtonPros => StyledButtonPros.colour};
  grid-column: span ${({ span = 1 }) => span};
  border: none;
  border-radius: 20px;
  &:hover {
    filter: brightness(1.2);
  }
`;

export default Button;
