import { observer } from "mobx-react-lite";
import React from "react";
import styled from "styled-components";
import { mathService } from "../../services/MathService";
import Button from "../button/Button";

const Body = () => {
  return (
    <StyledBody>
      <Button colour="light-gray" fontColour="black" span={3} onClick={() => mathService.reset()}>
        C
      </Button>
      <Button colour="orange" fontColour="white" span={1} onClick={() => mathService.setCount("/")}>
        /
      </Button>
      <Button colour="gray" fontColour="white" span={1} onClick={() => mathService.setCount("7")}>
        7
      </Button>
      <Button colour="gray" fontColour="white" span={1} onClick={() => mathService.setCount("8")}>
        8
      </Button>
      <Button colour="gray" fontColour="white" span={1} onClick={() => mathService.setCount("9")}>
        9
      </Button>
      <Button colour="orange" fontColour="white" span={1} onClick={() => mathService.setCount("x")}>
        x
      </Button>
      <Button colour="gray" fontColour="white" span={1} onClick={() => mathService.setCount("4")}>
        4
      </Button>
      <Button colour="gray" fontColour="white" span={1} onClick={() => mathService.setCount("5")}>
        5
      </Button>
      <Button colour="gray" fontColour="white" span={1} onClick={() => mathService.setCount("6")}>
        6
      </Button>
      <Button colour="orange" fontColour="white" span={1} onClick={() => mathService.setCount("-")}>
        -
      </Button>
      <Button colour="gray" fontColour="white" span={1} onClick={() => mathService.setCount("1")}>
        1
      </Button>
      <Button colour="gray" fontColour="white" span={1} onClick={() => mathService.setCount("2")}>
        2
      </Button>
      <Button colour="gray" fontColour="white" span={1} onClick={() => mathService.setCount("3")}>
        3
      </Button>
      <Button colour="orange" fontColour="white" span={1} onClick={() => mathService.setCount("+")}>
        +
      </Button>
      <Button colour="gray" fontColour="white" span={2} onClick={() => mathService.setCount("0")}>
        0
      </Button>
      <Button colour="gray" fontColour="white" span={1} onClick={() => mathService.setCount(".")}>
        .
      </Button>
      <Button colour="orange" fontColour="white" span={1} onClick={() => mathService.calculate()}>
        =
      </Button>
    </StyledBody>
  );
};

const StyledBody = styled.div`
  width: 170px;
  padding: 10px;
  display: grid;
  grid-template-columns: repeat(4, 35px);
  gap: 10px;
  background: black;
  border-style: solid;
  border-color: lightslategray;
  border-radius: 20px;
`;

export default observer(Body);
