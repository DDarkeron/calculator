import { observer } from "mobx-react-lite";
import React from "react";
import styled from "styled-components";
import { mathService } from "../../services/MathService";
import HistoryLine from "../historyOfCalculation/HistoryLine";
import Line from "../outputline/OutputLine";

const Header = () => {
  return (
    <StyledHeader span={4}>
      <HistoryLine resultOfCalculation={mathService.pile} />
      <Line resultOfCalculation={mathService.result} />
    </StyledHeader>
  );
};

interface StyledHeaderProps {
  span: number;
}
const StyledHeader = styled.div<StyledHeaderProps>`
  height: 30px;
  grid-column: span ${({ span = 1 }) => span};
  background: black;
  border-style: solid;
  border-color: lightslategray;
  border-radius: 20px;
  text-align: right;
  padding: 20px;
`;

export default observer(Header);
