import { observer } from "mobx-react-lite";
import Body from "./body/Body";
import Header from "./header/Header";
import StyledLayout from "./layout/Layout";

const App = () => {
  return (
    <div>
      <StyledLayout>
        <Header />
        <Body />
      </StyledLayout>
    </div>
  );
};

export default observer(App);
