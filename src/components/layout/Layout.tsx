import React, { FC } from "react";
import styled from "styled-components";

interface LayoutProps {}

const Layout: FC<LayoutProps> = ({ children }) => {
  return <StyledLayout></StyledLayout>;
};

const StyledLayout = styled.div`
  width: 200px;
  padding: 20px;
  display: grid;
  grid-template-columns: repeat(4, 41px);
  gap: 10px;
  background: black;
  border-radius: 20px;
`;

export default StyledLayout;
