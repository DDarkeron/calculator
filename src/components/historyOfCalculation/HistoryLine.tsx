import React, { FC } from "react";
import styled from "styled-components";

interface HistoryLineProps {
  resultOfCalculation: string;
}

const HistoryLine: FC<HistoryLineProps> = ({ resultOfCalculation }) => {
  return <StyledHistoryLine span={4}>{resultOfCalculation}</StyledHistoryLine>;
};

interface StyledHistoryLineProps {
  span: number;
}
const StyledHistoryLine = styled.div<StyledHistoryLineProps>`
  height: 1px;
  line-height: 1px;
  grid-column: span ${({ span = 1 }) => span};
  color: white;
  border-radius: 5px;
  text-align: right;
  padding: 10px;
`;

export default HistoryLine;
