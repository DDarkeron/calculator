import React, { FC } from "react";
import styled from "styled-components";

interface LineProps {
  resultOfCalculation: number;
}

const Line: FC<LineProps> = ({ resultOfCalculation }) => {
  return <StyledLine span={4}>{resultOfCalculation}</StyledLine>;
};

interface StyledLineProps {
  span: number;
}
const StyledLine = styled.div<StyledLineProps>`
  height: 1px;
  font-size: 30px;
  line-height: 1px;
  grid-column: span ${({ span = 1 }) => span};
  color: white;
  border-radius: 5px;
  text-align: right;
  padding: 10px;
`;

export default Line;
